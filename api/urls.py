from .views import HtmlContentViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'web', HtmlContentViewSet, basename='web')
urlpatterns = router.urls