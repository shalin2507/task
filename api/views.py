# Create your views here.
from rest_framework import mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from api.models import HtmlContent
from api.serializers import HtmlContentSerializer
from bs4 import BeautifulSoup
from collections import Counter


class HtmlContentViewSet(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    serializer_class = HtmlContentSerializer
    queryset = HtmlContent.objects.all()
    lookup_field = "name"

    @staticmethod
    def get_common_words(content, number):
        soup = BeautifulSoup(content)
        html_to_text = soup.get_text()

        # split() returns list of all the words in the string
        split_it = html_to_text.split()

        # Pass the split_it list to instance of Counter class.
        counter = Counter(split_it)

        # most_common() produces k frequently encountered
        # input values and their respective counts.
        return counter.most_common(number)

    @action(detail=True, url_path="common_words", url_name="common_words")
    def n_common_words(self, request, name=None):
        """Counts n common occures from single entry"""
        instance = self.get_object()
        try:
            number = int(request.query_params.get("n"))
        except (ValueError, TypeError):
            return Response(
                data="Valid number 'n' is required as query param.",
                status=status.HTTP_400_BAD_REQUEST,
            )

        most_occurence = self.get_common_words(instance.content)

        return Response(data=most_occurence, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, url_path="common_words_all", url_name="common_words_all")
    def common_words_all(self, request, name=None):
        """Counts n common occures from whole database"""
        content_list = self.get_queryset().values_list("content", flat=True)
        data = " ".join(content_list)

        try:
            number = int(request.query_params.get("n"))
        except (ValueError, TypeError):
            return Response(
                data="Valid number 'n' is required as query param.",
                status=status.HTTP_400_BAD_REQUEST,
            )

        most_occurence = self.get_common_words(data, 5)

        return Response(data=most_occurence, status=status.HTTP_400_BAD_REQUEST)
