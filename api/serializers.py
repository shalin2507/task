from api import models
from rest_framework import serializers


class HtmlContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.HtmlContent
        fields = "__all__"
