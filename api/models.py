from django.db import models

# Create your models here.
class HtmlContent(models.Model):
    name = models.CharField(verbose_name="Website Name", max_length=50, unique=True)
    content = models.TextField(verbose_name="website content")

    class Meta:
        indexes = [
            models.Index(
                fields=[
                    "name",
                ]
            ),
        ]
